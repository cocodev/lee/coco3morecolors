SOURCE_FILES = $(wildcard *.c)
all: moreClrs.dsk

%.o: %.c %.h
	cmoc --coco --compile --intermediate --verbose -o $@ $<

moreClrs.bin: main.o common.o textScreen.o palette.o interrupts.o
	cmoc --coco --intermediate --verbose -o moreClrs.bin --org=6000 -L/usr/local/share/cmoc/lib main.o common.o textScreen.o palette.o interrupts.o -lcmoc-crt-ecb -lcmoc-std-ecb 

moreClrs.dsk: moreClrs.bin moreClrs.bas
ifeq ("$(wildcard moreClrs.dsk)","")
	perl -e 'print chr(255) x (35*18*256)' > moreClrs.dsk
endif
	writecocofile --binary moreClrs.dsk moreClrs.bin
	writecocofile --ascii moreClrs.dsk moreClrs.bas

clean:
	find . -type f \( -name "*.o" -o -name "*.bin" -o -name "*.map" -o -name "*.link" -o -name "*.lst" -o -name "*.s" -o -name "desktop.ini" \) -delete
