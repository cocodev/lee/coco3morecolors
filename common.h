#ifndef _common_h_
#define _common_h_

#include <coco.h>

byte getKeyPress();
void mapPhysicalToLogicalMemory(byte physicalMemoryBlock, word logicalMemoryRegisterAddress);
void set640x200GraphicsMode(byte startingPhysicalMemoryBlock);
void setVideoStartingPhysicalMemoryBlock(byte startingPhysicalMemoryBlock);
void clearScreens();
void clearScreen(byte page);
void setCoCo12Width32();
void memsetWord(word *address, word wordToStore, word numberOfWords);

#define _PHYSICAL_48000_49FFF 0x24
#define _PHYSICAL_50000_51FFF 0x28
#define _PHYSICAL_58000_59FFF 0x2C
#define _PHYSICAL_60000_61FFF 0x30
#define _PHYSICAL_72000_73FFF 0x39
#define _LOGICAL_2000_3FFF 0xFFA1

#define poke(address, byteToStore) *(byte *)(address) = (byte)(byteToStore)
#define pokeWord(address, wordToStore) *(word *)(address) = (word)(wordToStore)
#define peek(address) *(byte *)(address)
#define peekWord(address) *(word *)(address)

#endif /* _common_h_ */
