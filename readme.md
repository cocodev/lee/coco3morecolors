# Setup Build Environemnt:
- Run `D:\devtools\cygwin64\Cygwin.bat` to start cygwin environment.
- In cygwin, change to morecolors folder: `cd /cygdrive/d/devtools/projects/morecolors/`

# Build:
- In cygwin, run `make`.
# Clean (remove all generated files)
- In cygwin, run `make clean`.
