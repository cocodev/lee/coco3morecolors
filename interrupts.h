#ifndef _interrupts_h_
#define _interrupts_h_

#include <coco.h>

interrupt void vsyncInterruptHandler();
void configureVsyncInterrupt();

#endif /* _interrupts_h_ */
