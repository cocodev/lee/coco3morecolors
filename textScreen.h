#ifndef _textScreen_h_
#define _textScreen_h_

#include <coco.h>

void drawCharacter(byte ascii, byte x, byte y, byte backColor, byte foreColor, byte shouldBlink);

#endif /* _textScreen_h_ */
