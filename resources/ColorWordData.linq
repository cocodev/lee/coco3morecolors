<Query Kind="Program" />

void Main()
{
    var data = new StringBuilder();
    for (byte color = 0; color <= 19; color++)
    {
        data.Append(GenerateColorWordMaskData(color));
        if (color < 19)
        {
            data.Append(",");
        }
        data.AppendLine();
    }
    data.ToString().Dump();
}

StringBuilder GenerateColorWordMaskData(byte color)
{
    var data = new StringBuilder();
    int color1 = colors[color, 0];
    int color2 = colors[color, 1];
    int color3 = colors[color, 2];

    data.AppendLine($"    {{   // Color {color:D2} = {color1}, {color2}, {color3}");
    for (var line = 0; line <= 7; line++)
    {
        int mask0 = (color1 << 14) | (color2 << 12) | (color3 << 10) | (color1 << 8) | (color2 << 6) | (color3 << 4) | (color1 << 2) | color2;
        int mask1 = (color2 << 14) | (color3 << 12) | (color1 << 10) | (color2 << 8) | (color3 << 6) | (color1 << 4) | (color2 << 2) | color3;
        int mask2 = (color3 << 14) | (color1 << 12) | (color2 << 10) | (color3 << 8) | (color1 << 6) | (color2 << 4) | (color3 << 2) | color1;
        data.Append($"        {{ 0b{ConvertIntegerTo16DigitBinary(mask0)}, 0b{ConvertIntegerTo16DigitBinary(mask1)}, 0b{ConvertIntegerTo16DigitBinary(mask2)} }}");
        if (line != 7)
        {
            data.Append(",");
        }
        data.AppendLine();
        int temp = color1;
        color1 = color3;
        color3 = color2;
        color2 = temp;
    }
    return data.Append("    }");
}

string ConvertIntegerTo16DigitBinary(int number)
{
    var binary = $"0000000000000000{Convert.ToString(number, 2)}";
    return binary.Substring(binary.Length - 16, 16);
}

byte[,] colorCombos = new byte[210, 2]
{
    {0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}, {0, 6}, {0, 7}, {0, 8}, {0, 9}, {0, 10}, {0, 11}, {0, 12}, {0, 13}, {0, 14}, {0, 15}, {0, 16}, {0, 17}, {0, 18}, {0, 19}, {1, 1},
    {1, 2}, {1, 3}, {1, 4}, {1, 5}, {1, 6}, {1, 7}, {1, 8}, {1, 9}, {1, 10}, {1, 11}, {1, 12}, {1, 13}, {1, 14}, {1, 15}, {1, 16}, {1, 17}, {1, 18}, {1, 19},
    {2, 2}, {2, 3}, {2, 4}, {2, 5}, {2, 6}, {2, 7}, {2, 8}, {2, 9}, {2, 10}, {2, 11}, {2, 12}, {2, 13}, {2, 14}, {2, 15}, {2, 16}, {2, 17}, {2, 18}, {2, 19},
    {3, 3}, {3, 4}, {3, 5}, {3, 6}, {3, 7}, {3, 8}, {3, 9}, {3, 10}, {3, 11}, {3, 12}, {3, 13}, {3, 14}, {3, 15}, {3, 16}, {3, 17}, {3, 18}, {3, 19},
    {4, 4}, {4, 5}, {4, 6}, {4, 7}, {4, 8}, {4, 9}, {4, 10}, {4, 11}, {4, 12}, {4, 13}, {4, 14}, {4, 15}, {4, 16}, {4, 17}, {4, 18}, {4, 19},
    {5, 5}, {5, 6}, {5, 7}, {5, 8}, {5, 9}, {5, 10}, {5, 11}, {5, 12}, {5, 13}, {5, 14}, {5, 15}, {5, 16}, {5, 17}, {5, 18}, {5, 19},
    {6, 6}, {6, 7}, {6, 8}, {6, 9}, {6, 10}, {6, 11}, {6, 12}, {6, 13}, {6, 14}, {6, 15}, {6, 16}, {6, 17}, {6, 18}, {6, 19},
    {7, 7}, {7, 8}, {7, 9}, {7, 10}, {7, 11}, {7, 12}, {7, 13}, {7, 14}, {7, 15}, {7, 16}, {7, 17}, {7, 18}, {7, 19},
    {8, 8}, {8, 9}, {8, 10}, {8, 11}, {8, 12}, {8, 13}, {8, 14}, {8, 15}, {8, 16}, {8, 17}, {8, 18}, {8, 19},
    {9, 9}, {9, 10}, {9, 11}, {9, 12}, {9, 13}, {9, 14}, {9, 15}, {9, 16}, {9, 17}, {9, 18}, {9, 19},
    {10, 10}, {10, 11}, {10, 12}, {10, 13}, {10, 14}, {10, 15}, {10, 16}, {10, 17}, {10, 18}, {10, 19},
    {11, 11}, {11, 12}, {11, 13}, {11, 14}, {11, 15}, {11, 16}, {11, 17}, {11, 18}, {11, 19},
    {12, 12}, {12, 13}, {12, 14}, {12, 15}, {12, 16}, {12, 17}, {12, 18}, {12, 19},
    {13, 13}, {13, 14}, {13, 15}, {13, 16}, {13, 17}, {13, 18}, {13, 19},
    {14, 14}, {14, 15}, {14, 16}, {14, 17}, {14, 18}, {14, 19},
    {15, 15}, {15, 16}, {15, 17}, {15, 18}, {15, 19},
    {16, 16}, {16, 17},  {16, 18}, {16, 19},
    {17, 17}, {17, 18}, {17, 19},
    {18, 18}, {18, 19},
    {19, 19}
};

byte[,] colors = new byte[20, 3]
{ // * == Most stable color combinations
    {0,0,0}, // Black, Black, Black
    {0,0,1}, // Black, Black, Red
    {0,0,2}, // Black, Black, Green
    {0,0,3}, // Black, Black, Blue
    {0,1,1}, // Black, Red,   Red
    {0,1,2}, // Black, Red,   Green
    {0,1,3}, // Black, Red,   Blue
    {0,2,2}, // Black, Green, Green
    {0,2,3}, // Black, Green, Blue
    {0,3,3}, // Black, Blue,  Blue
    {1,1,1}, // Red,   Red,   Red
    {1,1,2}, // Red,   Red,   Green
    {1,1,3}, // Red,   Red,   Blue
    {1,2,2}, // Red,   Green, Green
    {1,2,3}, // Red,   Green, Blue
    {1,3,3}, // Red,   Blue,  Blue
    {2,2,2}, // Green, Green, Green
    {2,2,3}, // Green, Green, Blue
    {2,3,3}, // Green, Blue,  Blue
    {3,3,3}  // Blue,  Blue,  Blue
};