<Query Kind="Program">
  <Namespace>System.Drawing</Namespace>
</Query>

Bitmap _image = new Bitmap(@"D:\devtools\projects\vsync\resources\cp437_8x8.png");

void Main()
{
    var characters = new StringBuilder();
    for(var ascii = 0; ascii <= 255; ascii++)
    {
        characters.Append(GenerateCharacterData(ascii));
        if (ascii < 255)
        {
            characters.Append(",");
        }
        characters.AppendLine();
    }
    characters.ToString().Dump();
}

StringBuilder GenerateCharacterData(int ascii)
{
    var character = new StringBuilder();
    var startX = (ascii % 32) * 8;
    var startY = (int)Math.Floor(ascii / 32d) * 8;
    character.AppendLine($"    {{   // ASCII {ascii:D3}");
    for (var y = startY; y < startY + 8; y++)
    {
        character.Append("        0b");
        for (var x = startX; x < startX + 8; x++)
        {
            character.Append(_image.GetPixel(x, y).R == 255 ? "11" : "00");
        }
        if (y < startY + 7)
        {
            character.Append(",");
        }
        character.AppendLine();
    }
    return character.Append("    }");
}
