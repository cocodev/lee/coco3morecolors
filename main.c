#include "textScreen.h"
#include "common.h"
#include "interrupts.h"
#include "palette.h"

int main()
{
    byte character = 1;

    setPalette();
    clearScreens();

    set640x200GraphicsMode(_PHYSICAL_50000_51FFF);
    configureVsyncInterrupt();

    byte backColor = 0;
    byte foreColor = 0;
    for(byte y = 0; y <= 24; y++)
    {
        for(byte x = 0; x <= 79; x++)
        {
            //foreColor = (byte)(rand() % 20);
            backColor = 0; // For now, until we get more a more stable screen
            character = (byte)(rand() % 255);
            drawCharacter(character++, x, y, backColor, foreColor, FALSE);
            if(character == 32 || character == 255 || character == 0)
            {
                character++;
            }
        }
        if(++backColor == 20)
        {
            backColor = 0;
        }
        if (++foreColor == 20)
        {
            foreColor = 0;
        }
    }

    while(TRUE) {}

    return 0;
}
