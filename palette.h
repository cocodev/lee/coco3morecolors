#ifndef _palette_h_
#define _palette_h_

#include <coco.h>

extern const word colorWordMasks[20][8][3];

void setPalette();

#endif /* _palette_h_ */
