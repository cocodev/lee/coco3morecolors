#include "interrupts.h"
#include "common.h"
#include "palette.h"

byte screen = 0;
byte vsyncCount = 0;
byte blinkMode = 0;
interrupt void vsyncInterruptHandler()
{
    switch(screen)
    {
        case 0:
            setVideoStartingPhysicalMemoryBlock(blinkMode == 0 ? _PHYSICAL_48000_49FFF : _PHYSICAL_50000_51FFF);
            break;
        case 1:
            setVideoStartingPhysicalMemoryBlock(_PHYSICAL_58000_59FFF);
            break;
        case 2:
            setVideoStartingPhysicalMemoryBlock(_PHYSICAL_60000_61FFF);
            break;
    }
    if (++screen == 3)
    {
        screen = 0;
    }
    ++vsyncCount;
    if(vsyncCount == 30)
    {
        vsyncCount = 0;
        if (blinkMode == 0)
        {
            blinkMode = 1;
        }
        else
        {
            blinkMode = 0;
        }
        
    }
    asm
    {
        LDA $FF02
    }
}

void configureVsyncInterrupt()
{
    asm
    {
        orcc    #$50

        lda     $FF00
        lda     $FF01
        anda    #$FE
        sta     $FF01

        lda     $FF02
        lda     $FF03
        ora     #$01
        sta     $FF03

        ldx     $FFF8
        lda     #$7E
        sta     0,x
        leay    vsyncInterruptHandler
        sty     1,x
        andcc   #$EF
        lda     $FF02
    }
}
