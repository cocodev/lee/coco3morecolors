#include <coco.h>
#include <cmoc.h>
#include "common.h"
#include "textScreen.h"

byte getKeyPress()
{
    byte key = waitkey(TRUE);
    if (key == 3)
    {
        exit(0);
    }
    return key;
}

void mapPhysicalToLogicalMemory(byte physicalMemoryBlock, word logicalMemoryRegisterAddress)
{
    poke(logicalMemoryRegisterAddress, physicalMemoryBlock);
}

    //   Name                     Active BP LPR    VRES  HRES   CRES  Width  Height
    //{ "Text 80x25",             TRUE,  0, 0b100, 0b11, 0b101, 0b01, 80*2,   25 }
    //{ "Text 80x28(+1 SL)",      TRUE,  0, 0b011, 0b11, 0b101, 0b01, 80*2,   29 }
    //{ "Gfx  640x200x4",         TRUE,  1, 0b000, 0b01, 0b111, 0b01, 640/4, 200 }

void set640x200GraphicsMode(byte startingPhysicalMemoryBlock)
{
    const byte BP   = 0b1;
    const byte LPR  = 0b000;
    const byte VRES = 0b01;
    const byte HRES = 0b111;
    const byte CRES = 0b01;
    byte videoModeValue = (BP << 7) | LPR;
    byte videoResolutionValue = (VRES << 5) | (HRES << 2) | CRES;
    poke(0xFF98, videoModeValue);       // VideoMode        - BP(7), Unused(6), DESCEN(5), MOCH(4), H50(3), LPR2-LPR0=(2-0)
    poke(0xFF99, videoResolutionValue); // VideoResolution  - Unused(7), VRES1-VRES0(6-5), HRES2-HRES0(4-2), CRES1-CRES0(1-0)
    poke(0xFF9A, 0b00000000);           // BorderColor      - Unused=(7-6), R1(5), G1(4), B1(3), R0(2), G0(1), B0(0)
    poke(0xFF9C, 0b00000000);           // VerticalScroll   - Unused=(7-4), SCEN(3), SC2-SC0(2-0)
    setVideoStartingPhysicalMemoryBlock(startingPhysicalMemoryBlock);
    // poke(0xFF9D, startingPhysicalMemoryBlock << 2); // VerticalOffset1  - Physical address bits 18-3=0b1100000000000000 (bits 2-0 always 0) == 0x60000
    // poke(0xFF9E, 0b00000000);           // VerticalOffset0  - 
    poke(0xFF9F, 0b00000000);           // HorizontalOffset - HE(6), X6-X0(6-0)
    poke(0xFF90, 0b01001100);           // Init0            - CoCo Bit(7), MMU(6), IEN(5), FEN(4), M3C(3), MC2(2), MC1-MC0(1-0)
}

void setVideoStartingPhysicalMemoryBlock(byte startingPhysicalMemoryBlock)
{
    poke(0xFF9D, startingPhysicalMemoryBlock << 2); // VerticalOffset1  - Physical address bits 18-3=0b1100000000000000 (bits 2-0 always 0) == 0x60000
    poke(0xFF9E, 0b00000000);                       // VerticalOffset0  - 
}

void clearScreens()
{
    clearScreen(_PHYSICAL_48000_49FFF);
    clearScreen(_PHYSICAL_50000_51FFF);
    clearScreen(_PHYSICAL_58000_59FFF);
    clearScreen(_PHYSICAL_60000_61FFF);
}

void clearScreen(byte startingPhysicalMemoryBlock)
{
    for (byte physicalBlock = startingPhysicalMemoryBlock; physicalBlock < startingPhysicalMemoryBlock + 4; physicalBlock++)
    {
        mapPhysicalToLogicalMemory(physicalBlock, _LOGICAL_2000_3FFF);
        memset(0x2000, 0b00000000, 0x2000);
    }
    mapPhysicalToLogicalMemory(_PHYSICAL_72000_73FFF, _LOGICAL_2000_3FFF);
    return;
}

void setCoCo12Width32()
{
    poke(0xE6,   0x00); // HRMODE = 0
    poke(0xE7,   0x00); // HRWIDTH = 32 (text mode 0)
    poke(0xFF90, 0xCC); // 
    poke(0xFF98, 0x00);
    poke(0xFF99, 0x00);
    poke(0xFF9A, 0x00);
    poke(0xFF9B, 0x00);
    poke(0xFF9C, 0x0F);
    poke(0xFF9D, 0xE0);
    poke(0xFF9E, 0x00);
    poke(0xFF9F, 0x00);
}

void memsetWord(word *address, word wordToStore, word numberOfWords)
{
    asm
    {
    pshs    u
    tfr     s,u
    leas    -2,s    end address

    ldd     4,u     start address (address)
    tfr     d,x
    addd    8,u     add number of words (numberOfWords) twice to get end address
    addd    8,u
    std     -2,u    store in local var

    ldd    6,u      word to write (wordToStore)
    bra    _memsetword_cond

_memsetword_loop
    std    ,x++

_memsetword_cond
    cmpx    -2,u            at end?
    bne     _memsetword_loop    no, continue

    ldd     4,u     return start address
    tfr     u,s
    puls    u
    }
}
